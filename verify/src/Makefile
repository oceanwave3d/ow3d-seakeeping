TEST_UTIL = CompareResults.cpp ReadUserTolerance.cpp PrintColorMessage.cpp
TEST_DERVS_SRC = TestDervs.cpp
TEST_BASE_SRC = TestBaseflow.cpp
TEST_GMHYDROS_SRC = TestGmHydros.cpp
TEST_UPWIND_SRC = TestUpwind.cpp
TEST_TIMOSH_SRC = TestTimoshenko.cpp
TEST01_SRC = Test01.cpp
TEST02_SRC = Test02.cpp
TEST03_SRC = Test03.cpp
TEST04_SRC = Test04.cpp
TEST05_SRC = Test05.cpp
TEST06_SRC = Test06.cpp
TEST07_SRC = Test07.cpp
TEST08_SRC = Test08.cpp
TEST09_SRC = Test09.cpp
TEST10_SRC = Test10.cpp
TEST11_SRC = Test11.cpp
TEST12_SRC = Test12.cpp
TEST13_SRC = Test13.cpp
TEST14_SRC = Test14.cpp
ALL_SRCS = $(TEST_UTIL) $(TEST_DERVS_SRC) $(TEST_BASE_SRC) $(TEST_GMHYDROS_SRC) $(TEST_UPWIND_SRC)\
$(TEST01_SRC) $(TEST02_SRC) $(TEST03_SRC) $(TEST04_SRC) $(TEST05_SRC) $(TEST06_SRC) $(TEST07_SRC)\
$(TEST08_SRC) $(TEST09_SRC) $(TEST10_SRC) $(TEST11_SRC) $(TEST12_SRC) $(TEST13_SRC) $(TEST_TIMOSH_SRC)

COMPILE.cpp = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) $(CCFLAGS) -c -std=c++17 -g  -Wall -fPIC -Wshadow
SHARED_LIB = -Wl,-rpath,${OW3D_DIR}/verify/src

.PHONY: all
all:Test01 Test02 Test03 Test04 Test05 Test06 Test07 Test08 Test09 Test10 Test11 Test12 Test13 Test14 TestDervs TestBaseflow TestGmHydros TestUpwind TestTimoshenko

libtests.so: PrintColorMessage.o ReadUserTolerance.o CompareResults.o
	gcc $^ -shared -o $@ 

TestDervs: $(TEST_DERVS_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
TestBaseflow: $(TEST_BASE_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
TestGmHydros: $(TEST_GMHYDROS_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
TestUpwind: $(TEST_UPWIND_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
TestTimoshenko: $(TEST_TIMOSH_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)	
Test01: $(TEST01_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test02: $(TEST02_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test03: $(TEST03_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test04: $(TEST04_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test05: $(TEST05_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test06: $(TEST06_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test07: $(TEST07_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test08: $(TEST08_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test09: $(TEST09_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test10: $(TEST10_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test11: $(TEST11_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test12: $(TEST12_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test13: $(TEST13_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)
Test14: $(TEST14_SRC:%.cpp=%.o) libtests.so
	g++ $< -L. -ltests -o $@ $(SHARED_LIB)	

PROGRAMS = TestDervs TestBaseflow TestGmHydros TestUpwind TestTimoshenko\
		   Test01 Test02 Test03 Test04 Test05 Test06 Test07 Test07 Test08 Test09 Test10 Test11 Test12 Test13 Test14

src/%.o: %.cpp TestHeaders.h
	$(COMPILE.cpp) -o $@ $<

ifneq ($(MAKECMDGOALS),clean)
include $(ALL_SRCS:%.cpp=%.d)
endif

%.d: %.cpp
	$(CXX) -M $(CPPFLAGS) $(CCFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

.PHONY: clean

clean:
	@rm -rf $(PROGRAMS)
	@rm -f libtests.so
	@rm -rf *.o
	@rm -rf *.d*
	@rm -rf ../tests/*/*/*.hdf
	@rm -rf ../tests/*/*/*_ow3dt
	@rm -rf ../tests/*/*/*_ow3df
	@rm -rf ../tests/*/*/ogen.*
	@rm -rf ../tests/*/*/plotStuff.*
	@rm -rf ../tests/*/*/ogmg.*
	@rm -rf ../tests/*/*/ow3d.ck
	@rm -rf ../tests/*/*/*.check
	@rm -rf ../tests/*/*.hdf
	@rm -rf ../tests/*/*_ow3dt
	@rm -rf ../tests/*/*_ow3df
	@rm -rf ../tests/*/ogen.*
	@rm -rf ../tests/*/plotStuff.*
	@rm -rf ../tests/*/ogmg.*
	@rm -rf ../tests/*/ow3d.ck
	@rm -rf ../tests/*/*.check
	@rm -rf ../tests/*/*/rap.*
	@rm -rf ../tests/ow3d/test13/patches
	@echo "erased all .d, , .hdf, and .o files..."
