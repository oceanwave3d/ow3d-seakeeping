
#ifndef __TEST_HEADERS__
#define __TEST_HEADERS__

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>

namespace OW3DSeakeepingTESTS
{
    int CompareResults(std::string SExpected, std::string SActual, double pres, unsigned int skip = 0);
    void PrintColorMessage(std::string message, char p);
    double ReadUserTolerance(int argc, char *argv[]);
}

#endif