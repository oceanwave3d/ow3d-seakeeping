  *   
  * --------------------------------------------------------------------------------------------
  * This scrip generates the overlapping grid for the flexible barge using "ogen". The script
  * loads the hdf file containing the grid components, made by "mbuilder" hyperbolic grid
  * generator. The background grid, and all required transformations are defined in this script.
  * --------------------------------------------------------------------------------------------
  * 
  * - GENERAL VARIABLES
  * 
  $scl      = 1;
  $L        = 2.445 * $scl; # The length of the ship 
  $B        = 0.6  * $scl; # The beam   of the ship
  $res      = 1;  # resolutions
  $resHull  = 1;
  $resBow   = 1;
  $resStern = 1;
  *
  * 
  * - VARIABLES FOR THE CYLINDER GRID
  *  
  $Cyln_rin = $B;
  $Cyln_rot = 15 * $L;
  $Cyln_atp = 0;
  $Cyln_abt = -2 * $L;  
  $Cyln_lnc = int(30 * $res);
  $Cyln_lnv = int(20 * $res);
  $Cyln_lnr = int(50 * $res);
  *
  * - VARIABLES FOR THE CORE GRID  
  * 
  $Core_Cxl = -1.25 * $Cyln_rin;
  $Core_Cxr =  1.25 * $Cyln_rin;
  $Core_Cyb = $Cyln_abt;
  $Core_Cyt =  0;
  $Core_Czf =  1.25 * $Cyln_rin;
  $Core_Czk =  0;
  $Core_ln1 =  int(28 * $res);
  $Core_ln2 =  $Cyln_lnv;
  $Core_ln3 =  int(8 * $res);
  *
  $Rect_ln1 = int(15 * $res);
  $Rect_ln2 = int(10 * $res);
  $Rect_ln3 = int(50 * $res);
  *
  * 
  * - NOTE : SHARE VALUES :
  * 
  * 6 : free surface
  * 7 : body
  * 8 : wall
  * 9 : symmetry plane
  * 
  * - NOTE : BOUNDARY TAGS :
  *  
  * 1 : free surface
  * 2 : (impermeable)
  * 3 : body 
  * 4 : wall
  * 5 : symmetry
  * ----------------------------------------------------------------------
  *                   GENERATE THE BACKGROUND GRID
  * ----------------------------------------------------------------------
  create mappings
  * **** The core grid ****
  Box
  set corners
  $Core_Cxl $Core_Cxr $Core_Cyb $Core_Cyt $Core_Czf $Core_Czk 
  lines
  $Core_ln1 $Core_ln2 $Core_ln3
  share
  0 0 8 0 0 9
  boundary conditions
  0 0 2 0 0 5
  mappingName
  core
  exit
  * 
  * **** The cylinder grid ****
  *   
  Cylinder
  orientation
  0 2 1
  bounds on the radial variable
  $Cyln_rin $Cyln_rot
  bounds on the axial variable
  $Cyln_abt $Cyln_atp 
  exit
  rotate/scale/shift
  rotate
  90 1
  0 0 0
  mappingName
  fulCylinder
  exit
  reparameterize
  transform which mapping?
  fulCylinder
  restrict parameter space
  set corners
  1.25 1.75 0. 1 0. 1  
  exit
  boundary conditions
  5 5 2 1 0 4
  share
  9 9 8 6 0 0
  lines
  $Cyln_lnc $Cyln_lnv $Cyln_lnr
  mappingName
  cylinder
  exit
  * ----------------------------------------------------------------------
  *             LOAD AND TRANSFORM THE HYPERBOLIC COMPONENTS
  * ----------------------------------------------------------------------
  open a data-base
  fbarge_hyped.hdf
  open an old file read-only
  get all mappings from the data-base
  rotate/scale/shift
  transform which mapping?
  MIDSEC
    rotate
    -90 0
    exit
  scale
  $scl $scl $scl
  lines
  $hull_l1 = int(50 * $resHull)+1;
  $hull_l2 = int(55 * $resHull);
  $hull_l3 = int(21 * $resHull);
  $hull_l1 $hull_l2 $hull_l3
  mappingName
  Mss
  exit
  *
  *
  open a data-base
  fbarge_hyped.hdf
  open an old file read-only
  get all mappings from the data-base
  rotate/scale/shift
  transform which mapping?
  AFT
    rotate
    -90 0
    exit
  scale
  $scl $scl $scl
  lines
  $bow_l1 = int(31 * $resBow);
  $bow_l2 = int(51 * $resBow);
  $bow_l3 = int(21 * $resBow);
  $bow_l1 $bow_l2 $bow_l3
  mappingName
  Afts
  exit
  *
  *
  open a data-base
  fbarge_hyped.hdf
  open an old file read-only
  get all mappings from the data-base
  rotate/scale/shift
  transform which mapping?
  FORE
    rotate
    -90 0
    exit
  scale
  $scl $scl $scl
  lines
  $stern_l1 = int(30 * $resStern);
  $stern_l2 = int(60 * $resStern)+1;
  $stern_l3 = int(21 * $resStern);
  $stern_l1 $stern_l2 $stern_l3
  mappingName
  Fores
  exit
  *
  *
reparameterize
  transform which mapping?
  Mss
  set corners
  0 0.5 0. 1. 0. 1.
  share
  6 9 0 0 7 0
  boundary conditions
  1 5 0 0 3 0
  mappingName
  Msbc
  exit
  * 
  reparameterize
  transform which mapping?
  Afts
  set corners
  0. 1. 0 0.5 0. 1.
  share
  0 6 0 9 7 0
  boundary conditions
  0 1 0 5 3 0
  mappingName
  Aftbc
  exit
  * 
  * 
  reparameterize
  transform which mapping?
  Fores
  set corners
  0 1 0.5 1 0 1
  share
   0 6 9 0 7 0
  boundary conditions
   0 1 5 0 3 0
  mappingName
  Forebc
  exit
* ----------------------------------------------------------------------
  *                     STRETCH THE COMPONENT GRIDS
  * ----------------------------------------------------------------------
  stretch coordinates
  transform which mapping?
  core
  stretch
  specify stretching along axis=1
  layers
  1
  6 6 1
  exit
  exit
  mappingName
  strCore
  exit
  * 
  stretch coordinates
  transform which mapping?
  cylinder
  stretch
  specify stretching along axis=2
  layers
  1
  8 15 0
  exit
  specify stretching along axis=1
  layers
  1
  6 6 1
  exit 
  exit
  mappingName
  strCylinder
  exit
  * 
  stretch coordinates
  transform which mapping?
  Msbc
  stretch
  specify stretching along axis=1
  layers
  3
  5 6 0
  5 6 1
  2 3 0.5
  exit
  exit
  mappingName
  strHull
  exit
  *
  stretch coordinates
  transform which mapping?
  Aftbc
  stretch
  specify stretching along axis=1
  layers
  3
  3 3 0
  2 2 1
  3 5 0.25
  exit
  exit
  mappingName
  strAft
  exit 
  * 
  stretch coordinates
  transform which mapping?
  Forebc
  stretch
  *specify stretching along axis=0
  *layers
  *2
  *6 5 0.9
  *3 3 0
  *3 5 0.0
  *exit
  *specify stretching along axis=2
  *layers
  *1
  *1 5 0
  *exit
  specify stretching along axis=1
  layers
  3
  3 3 0
  2 2 1
  4 5 0.85
  exit
  exit
  mappingName
  strFore
  exit
  * ----------------------------------------------------------------------
  * View mappings
  * ----------------------------------------------------------------------
  view mappings
  *set view:0 -0.114007 -0.0574453 0 1.7866 -0.642788 -0.383022 0.663414 0 0.866025 0.5 -0.766044 0.321394 -0.55667
  *strCylinder
  *strCore
  strFore
  strAft
  strHull
  colour boundaries by share value
  #pause
  colour boundaries by boundary condition number
  #pause
  erase and exit
  exit
  * ----------------------------------------------------------------------
  * Build overlapping grid 
  * ----------------------------------------------------------------------
  generate an overlapping grid
  strCylinder
  strCore
  strFore
  strAft
  strHull
  done
  change parameters
  interpolation type
  implicit for all grids 
  order of accuracy
  fourth order
  ghost points
  all
  *1 1 1 1 1 1 
  2 2 2 2 2 2
  exit
  compute overlap
  exit
  save an overlapping grid
  fbarge.hdf
  exit
  exit
