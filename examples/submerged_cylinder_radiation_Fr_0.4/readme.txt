To compare the results with the closed-form solutions from

G. Wu, Hydrodynamic forces on a submerged cylinder advancing in 
water waves of ﬁnite depth, J. Fluid Mech. 224 (1991) 645–659.

run this geometry with the variable "GX_WU_BCS = true" in the OW3DConstants.cpp
