To compare the results with the closed-form solutions from

G. Wu, R. Eatock Taylor, The hydrodynamic force on an oscillating ship 
with low forward speed, J. Fluid Mech. 211 (1990) 333–353.

run this geometry with the variable "GX_WU_BCS = true" in the OW3DConstants.cpp
