To compare the results with the closed-form solutions from

G. Wu, R. Eatock Taylor, Radiation and diffraction of water waves 
by a submerged sphere at forward speed, 
Proc. R. Soc. Lond. A: Math. Phys. Sci. 417 (1853) (1988) 433–461,

run this geometry with the variable "GX_WU_BCS = true" in the OW3DConstants.cpp
