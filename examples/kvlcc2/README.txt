
1. To build the grid for the tanker:

-----------------------
rap      sternClean
rap      wholeClean
mbuilder bowHype
mbuilder hullHype
mbuilder sternHype
ogen     overlap
-----------------------

By this an overlapping grid 
is made with the name: kvlcc2.hdf

2. To see the overset grid:

----------------------------
plotStuff <name of the grid> 
----------------------------
