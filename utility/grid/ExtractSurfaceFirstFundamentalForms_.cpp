#include "Grid.h"
#include "OW3DUtilFunctions.h"

OW3DSeakeeping::FundamentalForms ComputeFundamentals(RealArray X, RealArray Y, RealArray Z, int JacobianSign)
{
    RealArray coefficients = OW3DSeakeeping::CalculateFirstCoefficients(4);
    int S0 = X.getLength(0);
    int S1 = X.getLength(1);
    int S2 = X.getLength(2); // Y is upward
    int N0, N1;

    if (S0 == 1)
    {
        N0 = S1;
        N1 = S2;
    }
    else if (S1 == 1)
    {
        N0 = S0;
        N1 = S2;
    }
    else if (S2 == 1)
    {
        N0 = S0;
        N1 = S1;
    }
    else
    {
        throw runtime_error(OW3DSeakeeping::GetColoredMessage("\t Error (OW3D), Grid::ExtractSurfaceFirstFundamentalForms_.cpp.\n\t The coordinates of only 1 side should be given.", 0));
    }

    // Convention: u is along axis0, v is along axis 1

    double dv = 1.0 / (N0 - 1);
    double du = 1.0 / (N1 - 1);
    X.reshape(N0, N1);
    Y.reshape(N0, N1);
    Z.reshape(N0, N1);
    Range All;
    RealArray Xu(N0, N1), Yu(N0, N1), Zu(N0, N1);
    Xu = 0, Yu = 0, Zu = 0;
    RealArray Xv(N0, N1), Yv(N0, N1), Zv(N0, N1);
    Xv = 0, Yv = 0, Zv = 0;
    // Compute 1D derivatives along u ---------------------------------------------------
    OW3DSeakeeping::Compute1DDerivatives(X, Xu, 0, N1 - 1, coefficients);
    OW3DSeakeeping::Compute1DDerivatives(Y, Yu, 0, N1 - 1, coefficients);
    OW3DSeakeeping::Compute1DDerivatives(Z, Zu, 0, N1 - 1, coefficients);
    Xu *= du, Yu *= du, Zu *= du;
    // ---------------------------------------------------------------------------------
    RealArray tX = transpose(X);
    RealArray tXv = transpose(Xv);
    RealArray tY = transpose(Y);
    RealArray tYv = transpose(Yv);
    RealArray tZ = transpose(Z);
    RealArray tZv = transpose(Zv);
    // Compute 1D derivatives along v ---------------------------------------------------
    OW3DSeakeeping::Compute1DDerivatives(tX, tXv, 0, N0 - 1, coefficients);
    OW3DSeakeeping::Compute1DDerivatives(tY, tYv, 0, N0 - 1, coefficients);
    OW3DSeakeeping::Compute1DDerivatives(tZ, tZv, 0, N0 - 1, coefficients);
    tXv *= dv, tYv *= dv, tZv *= dv;
    Xv = transpose(tXv);
    Yv = transpose(tYv);
    Zv = transpose(tZv);

    OW3DSeakeeping::FundamentalForms fundamentals;
    fundamentals.E = Xu * Xu + Yu * Yu + Zu * Zu;
    fundamentals.F = Xu * Xv + Yu * Yv + Zu * Zv;
    fundamentals.G = Xv * Xv + Yv * Yv + Zv * Zv;
    fundamentals.H = sqrt(fundamentals.E * fundamentals.G - fundamentals.F * fundamentals.F);
    fundamentals.n1 = (Yu * Zv - Zu * Yv) / fundamentals.H * JacobianSign;
    fundamentals.n2 = (Zu * Xv - Xu * Zv) / fundamentals.H * JacobianSign;
    fundamentals.n3 = (Xu * Yv - Yu * Xv) / fundamentals.H * JacobianSign;
    fundamentals.Xu = Xu;
    fundamentals.Yu = Yu;
    fundamentals.Zu = Zu;
    fundamentals.Xv = Xv;
    fundamentals.Yv = Yv;
    fundamentals.Zv = Zv;
    fundamentals.X = X;
    fundamentals.Y = Y;
    fundamentals.Z = Z;

    return fundamentals;
}

void OW3DSeakeeping::Grid::ExtractSurfaceFirstFundamentalForms_()
{
    for (unsigned int surface = 0; surface < gridData_.boundariesData.exciting.size(); surface++)
    {
        const Single_boundary_data &be = gridData_.boundariesData.exciting[surface];
        const MappedGrid &mg = (*cg_)[be.grid];
        const RealArray &v = mg.vertex();
        const RealArray &vbn = mg.vertexBoundaryNormal(be.side, be.axis);
        const vector<Index> &Is = be.surface_indices;
        Mapping &mapping = mg.mapping().getMapping();
        int jsg = mapping.getSignForJacobian();
        RealArray x(Is[0], Is[1], Is[2]), y(Is[0], Is[1], Is[2]), z(Is[0], Is[1], Is[2]);
        x = v(Is[0], Is[1], Is[2], axis1);
        y = v(Is[0], Is[1], Is[2], axis2); // Note : "y" vertical up
        z = 0.0;
        if (mg.numberOfDimensions() == 3)
        {
            z = v(Is[0], Is[1], Is[2], axis3);
        }
        gridData_.body_fundamentals.push_back(ComputeFundamentals(x, y, z, jsg));
        gridData_.body_fundamentals[surface].n1_OV = vbn(Is[0], Is[1], Is[2], axis1);
        gridData_.body_fundamentals[surface].n2_OV = vbn(Is[0], Is[1], Is[2], axis2);
        gridData_.body_fundamentals[surface].n3_OV = vbn(Is[0], Is[1], Is[2], axis3);
    }
    //
    for (unsigned int surface = 0; surface < gridData_.boundariesData.free.size(); surface++)
    {
        const Single_boundary_data &bf = gridData_.boundariesData.free[surface];
        const MappedGrid &mg = (*cg_)[bf.grid];
        const RealArray &v = mg.vertex();
        const RealArray &vbn = mg.vertexBoundaryNormal(bf.side, bf.axis);
        const vector<Index> &Is = bf.surface_indices;
        Mapping &mapping = mg.mapping().getMapping();
        int jsg = mapping.getSignForJacobian();
        RealArray x(Is[0], Is[1], Is[2]), y(Is[0], Is[1], Is[2]), z(Is[0], Is[1], Is[2]);
        x = v(Is[0], Is[1], Is[2], axis1);
        y = v(Is[0], Is[1], Is[2], axis2); // Note : "y" vertical up
        z = 0.0;
        if (mg.numberOfDimensions() == 3)
        {
            z = v(Is[0], Is[1], Is[2], axis3);
        }

        gridData_.free_fundamentals.push_back(ComputeFundamentals(x, y, z, jsg));
        gridData_.free_fundamentals[surface].n1_OV = vbn(Is[0], Is[1], Is[2], axis1);
        gridData_.free_fundamentals[surface].n2_OV = vbn(Is[0], Is[1], Is[2], axis2);
        gridData_.free_fundamentals[surface].n3_OV = vbn(Is[0], Is[1], Is[2], axis3);

        // Reshpae the arrays
        int S0 = x.getLength(0);
        int S1 = x.getLength(1);
        int S2 = x.getLength(2);
        int N0, N1;

        if (S0 == 1)
        {
            N0 = S1;
            N1 = S2;
        }
        else if (S1 == 1)
        {
            N0 = S0;
            N1 = S2;
        }

        gridData_.free_fundamentals[surface].n1_OV.reshape(N0, N1);
        gridData_.free_fundamentals[surface].n2_OV.reshape(N0, N1);
        gridData_.free_fundamentals[surface].n3_OV.reshape(N0, N1);
    }
}
