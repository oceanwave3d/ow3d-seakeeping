// Calculate first-derivative coefficients.
// =============================================================================
// Name: calculate_first_coefficients.cpp
// Purpose: Calculate the first-derivative coefficients.
// Author: R. W. Read
// Version: 1
// Additional comments:
// =============================================================================

#include "OW3DUtilFunctions.h"

RealArray OW3DSeakeeping::CalculateFirstCoefficients(const int order)
{
  const int stencil_point_count = order + 1;
  RealArray stencil_point_locations(stencil_point_count);
  for (int i = 0; i < stencil_point_count; ++i)
  {
    stencil_point_locations(i) = i;
  }
  Index If(1, 1); // first derivative line in array returned by calculate_coefficients
  Index Is(0, stencil_point_count);
  RealArray coefficients(Is, Is);
  for (int j = 0; j < stencil_point_count; ++j)
  {
    Index Ic(j, 1); // first derivative line in coefficients
    coefficients(Ic, Is) = CalculateCoefficients(j, stencil_point_locations, stencil_point_count - 1)(If, Is);
  }
  return coefficients;
}
